import React, { Component } from 'react';
import ls from 'local-storage';
import { FaCheck } from 'react-icons/fa';
import { FaTimes } from 'react-icons/fa';
import { FaPause } from 'react-icons/fa';

import RoundResultComponent from './RoundResultComponent';
import config from '../config/config';
const RoundState = {
    Start: "Start",
    InProgress: "InProgress",
    Pause: "Pause",
    Finish: "Finish"
};

class RoundComponent extends Component {
    constructor(props) {
        super(props);
        const { team, time, roundState } = ls.get("CurrentRound");
        console.log(config);
        this.state = {
            team: team,
            time: time,
            roundState: roundState || RoundState.Start,
            words: ls.get('words'),
            wordsResult: []
        }
        this.startRound = this.startRound.bind(this);
        this.generateWord = this.generateWord.bind(this);
    }

    wordCheck(value) {
        if (value) {
           this.state.wordsResult.push({word:this.state.word, result: true});
        } else {
            this.state.wordsResult.push({word:this.state.word, result: false});                
        }
        this.generateWord();
    }

    generateWord() {
        const words = this.state.words;
        const word = words.length * Math.random();
        this.setState({
            word: words[Math.floor(word)]
        });
    }

    componentDidMount() {
        if (!this.state.team) {
            this.props.history.push('/gameconfig');
            return;
        }
        this.generateWord();
    }

    startRound() {
        var scope = this;
        this.setState({
            roundState: RoundState.InProgress
        });
        this.interval = setInterval(() => {
            if (this.state.time > 0) {
                this.tick();
            } else {
                clearInterval(scope.interval);
                this.setState({
                    roundState: RoundState.Finish
                });
            }
        }, 1000);
    }

    tick() {
        this.setState(state => ({
            time: state.time - 1
        }));
    }

    render() {
        const roundState = this.state.roundState;
        const time = this.state.time || "00";
        if (roundState === RoundState.Start) {
            return (
                <div>
                    <h1 style={{ color: this.state.team.color }}>{this.state.team.name}</h1>
                    <span className='start-round-button' onClick={this.startRound}>
                        Start
                    </span>
                </div>
            )
        } else if (roundState === RoundState.InProgress) {
            return (
                <>
                    <div className='timer'>
                        <span>{time}</span>
                        <FaPause preserveAspectRatio='xMidYMin' className='pause' />
                    </div>
                    <div className='word'>
                        <h1>{this.state.word.name}</h1>
                    </div>
                    <div className='round-controls'>
                        <span>
                            <FaCheck onClick={() => {this.wordCheck(true)}} />
                        </span>
                        <span>
                            <FaTimes onClick={() => {this.wordCheck(false)}}/>
                        </span>
                    </div>
                </>
            )
        } else {
            return (
               <RoundResultComponent items={this.state.wordsResult}/>
            )
        }
    }
}

export default RoundComponent;
