import React, { Component } from 'react';
import { FaCheck } from 'react-icons/fa';
import { FaTimes } from 'react-icons/fa';
const RoundResultComponent = ({ items }) => {
    return (
        <div>
            <h1>Round result</h1>
            {items.map(item => {
                return (
                    <div key={item.word.id} className='results'>
                        {item.word.name}
                        {item.result ? <FaCheck className='green' /> : <FaTimes className='red' />}
                    </div>
                )
            })}
            <h1>Total: {items.filter(item => item.result).length}</h1>
        </div>
    );
};

export default RoundResultComponent;