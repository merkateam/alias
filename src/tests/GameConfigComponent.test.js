import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import GameConfig from '../components/GameConfigComponent';

let container = null;
beforeEach(() => {
    container = document.createElement("div");
    document.body.appendChild(container);
});

afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
});

it("add team", () => {
    act(() => {
        render(<GameConfig />, container);
    });
    addTeam(container, "Team1");
    addTeam(container, "Team2");
    addTeam(container, "Team3");
    addTeam(container, "Team4");
    var teams = container.querySelectorAll('.team');
    expect(teams.length).toEqual(4); 
});

it("remove team", () => {
    act(() => {
        render(<GameConfig />, container);
    });
    addTeam(container, "Team1");
    addTeam(container, "Team2");
    removeTeam(container);
    removeTeam(container);
    addTeam(container, "Team2");
    var teams = container.querySelectorAll('.team');
    expect(teams.length).toEqual(1);  
});


function removeTeam(container) {
    var removeItem = container.querySelector('.team');
    act(() => {
        removeItem.dispatchEvent(new MouseEvent("click", { bubbles: true }));
    });
}

function addTeam(container, name) {
    var teamInput = container.querySelector('.team-input');
    teamInput.value =  name;
    var addTeamButton = container.querySelector('.team-add-button');
    act(() => {
        addTeamButton.dispatchEvent(new MouseEvent("click", { bubbles: true }));
    });
}

