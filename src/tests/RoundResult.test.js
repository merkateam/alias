import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act, scryRenderedDOMComponentsWithClass } from "react-dom/test-utils";
import RoundResult from '../components/RoundResultComponent';

let container = null;
const resultItems = [
    {
        word: {
            id: "1",
            name: "Test"
        },
        result: true
    },
    {
        word: {
            id: "2",
            name: "Test"
        },
        result: true
    },
    {
        word: {
            id: "3",
            name: "Test"
        },
        result: true
    },
    {
        word: {
            id: "4",
            name: "Test"
        },
        result: false
    },
];
beforeEach(() => {
    container = document.createElement("div");
    document.body.appendChild(container);
});

afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
});

it("results total count", () => {
    act(() => {
        render(<RoundResult items={resultItems} />, container);
    });
    var results = container.querySelectorAll('.results');
    expect(results.length).toEqual(resultItems.length);
});

it("correct count", () => {
    act(() => {
        render(<RoundResult items={resultItems} />, container);
    });
    var results = container.querySelectorAll('.green');
    expect(results.length).toEqual(resultItems.filter(item => item.result).length);
});

it("fail count", () => {
    act(() => {
        render(<RoundResult items={resultItems} />, container);
    });
    var results = container.querySelectorAll('.red');
    expect(results.length).toEqual(resultItems.filter(item => !item.result).length);
});
